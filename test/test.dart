import 'package:test/test.dart';
import '../bin/main.dart';

void main() {
    test('Adding 5 and 3 must equal 8.', () {
        expect(add(5, 3), 8);
    });

    group('[1] Switch numbers.', () {
        num num1 = 4;
        num num2 = 3;
        Map<String, num> result = switchNumber(num1, num2);

        test('The value of num1 becomes the value of num2.', () {
            expect(result['num1'], num2);
        });

        test('The value of num2 becomes the value of num1.', () {
            expect(result['num2'], num1);
        });
    });

    group('[2] Count letters in a sentence.', () {
        String validLetter = 'o';
        String invalidLetter = 'abc';
        String sentence = 'The quick brown fox jumps over the lazy dog';

        test('Invalid letter returns null.', () {
            int? result = countLetter(invalidLetter, sentence);
            expect(result, null);
        });

        test('Valid letter returns number of occurrence.', () {
            int? result = countLetter(validLetter, sentence);
            expect(result, 4);
        });
    });

    group('[3] Check palindrome.', () {
        String palindrome = 'Was it a car or a cat I saw';
        String notPalindrome = 'Hello world!';

        test('Returns true if a phrase is a palindrome.', () {
            bool result = isPalindrome(palindrome);
            expect(result, true);
        });

        test('Returns false if a phrase is not a palindrome.', () {
            bool result = isPalindrome(notPalindrome);
            expect(result, false);
        });
    });

    group('[4] Check isogram.', () {
        String isogram = 'Machine';
        String notIsogram = 'Hello';

        test('Returns true if a word is an isogram.', () {
            bool result = isIsogram(isogram);
            expect(result, true);
        });

        test('Returns false if a word is not an isogram.', () {
            bool result = isIsogram(notIsogram);
            expect(result, false);
        });
    });

    group('[5] Purchase goods.', () {
        num price = 109.4356;
        num discountedPrice = price * 0.8;
        num roundedPrice = num.parse(discountedPrice.toStringAsFixed(2));

        test('Returns undefined for students aged below 13.', () {
            num? result = purchase(12, price);
            expect(result, null);
        });

        test('Returns discounted price (rounded off) for students aged 13 to 21.', () {
            num? result = purchase(15, price);
            expect(result, roundedPrice);
        });

        test('Returns discounted price (rounded off) for senior ctestizens.', () {
            num? result = purchase(72, price);
            expect(result, roundedPrice);
        });

        test('Returns price (rounded off) for people aged 22 to 64.', () {
            num? result = purchase(34, price);
            expect(result, num.parse(price.toStringAsFixed(2)));
        });
    });
    group('[6] Find hot categories.', () {
        List<Map<String, dynamic>> items = [
            { 'id': 'tltry001', 'name': 'soap', 'stocks': 14, 'category': 'toiletries' },
            { 'id': 'tltry002', 'name': 'shampoo', 'stocks': 8, 'category': 'toiletries' },
            { 'id': 'tltry003', 'name': 'tissues', 'stocks': 0, 'category': 'toiletries' },
            { 'id': 'gdgt001', 'name': 'phone', 'stocks': 0, 'category': 'gadgets' },
            { 'id': 'gdgt002', 'name': 'monitor', 'stocks': 0, 'category': 'gadgets' }
        ];

        test('Returns item categories without stocks.', () {
            List<String> result = findHotCategories(items);
            expect(result, ['toiletries', 'gadgets']);
        });
    });

    group('[7] Find flying voters.', () {
        List<String> candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
        List<String> candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

        test('Returns the array of flying voters.', () {
            List<String> result = findFlyingVoters(candidateA, candidateB);
            expect(result, ['LIWf1l', 'V2hjZH']);
        });
    });

    group('[8] Calculate ad efficiency.', () {
        List<Map<String, dynamic>> adCampaigns = [
            { 'brand': 'Brand X', 'expenditure': 12345.89, 'customersGained': 4879 },
            { 'brand': 'Brand Y', 'expenditure': 22456.17, 'customersGained': 6752 },
            { 'brand': 'Brand Z', 'expenditure': 18745.36, 'customersGained': 5823 }
        ];

        test('Returns the sorted ad efficiency.', () {
            List<Map<String, dynamic>> result = calculateAdEfficiency(adCampaigns);
            expect(result, [
                { 'brand': 'Brand X', 'adEfficiency': 39.51922461645131 },
                { 'brand': 'Brand Z', 'adEfficiency': 31.063687227132476 },
                { 'brand': 'Brand Y', 'adEfficiency': 30.06746030155632 }
            ]);
        });
    });
}